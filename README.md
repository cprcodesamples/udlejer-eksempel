## This is an example of calling the CPR UDLEJER service...
This is an example program that calls a demonstration version of
the UDLEJER service.

### Prerequisites
- Java 8 or higher
- Maven 3.6.3 or higher
- Git

### Clone project
Clone this project to a folder on you pc:

`git clone https://bitbucket.org/cprcodesamples/udlejer-eksempel.git`

### How to build
Open a CMD (Windows) or shell (Linux). Change dir to the dir you cloned the project to and run the following command:
 
`mvn clean compile package`

### How to run
After compile has finished program can be run with:

`java -jar target\udlejer-eksempel.jar  CBOLBG02 Test*123`

### Documentation

Documentation for Udlejerservice can be found here:

https://cprservicedesk.atlassian.net/wiki/spaces/CPR/pages/873332737/Servicespecifikation+til+udlejerorganisationer
