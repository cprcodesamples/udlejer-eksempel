package dk.cpr;

import dk.cpr.gctp.Gctp;

/**
 * @author Rune Molin, rmo@nine.dk
 **/
public interface GctpServiceFacade {
  /**
   * @param username username
   * @param password password
   * @throws IllegalStateException If no token was received
   * @throws IllegalStateException if HTTP response from server is anything other than OK
   * @throws RuntimeException      On any I/O errors or errors parsing the response
   */
  void login(String username, String password);

  /**
   * Invoke a GCTP search service
   *
   * @param serviceName Name of service to invoke
   * @param keyvalues   Array of key names followed by their value. Total length of array must be an even number
   * @return Response from service as a {@link Gctp} structure
   * @throws IllegalArgumentException if length of keyvalues is not an even number
   * @throws IllegalStateException    if HTTP response from server is anything other than OK
   * @throws RuntimeException         On any I/O errors or errors parsing the response
   */
  Gctp invoke(String serviceName, String... keyvalues);
}
