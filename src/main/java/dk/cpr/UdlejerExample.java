/*
 * Copyright (c) 2016 CSC Danmark A/S
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 *
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 *
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dk.cpr;

import dk.cpr.facade.DefaultGctpServiceFacade;
import dk.cpr.gctp.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Rune Molin, rmo@nine.dk
 */
public class UdlejerExample {
  private static final Logger logger = LoggerFactory.getLogger(UdlejerExample.class);
  private static final String HOST = "https://gctp-demo.cpr.dk/cpr-online-gctp/gctp";

  public static void main(String... args) {
    try {
      if (args.length < 2) {
        throw new IllegalArgumentException("Expected two arguments: username and password");
      }

      String username = args[0];
      String password = args[1];

      new UdlejerExample().run(new DefaultGctpServiceFacade(HOST), username, password);
    } catch (Exception e) {
      logger.error("Unexpected error", e);
    }
  }

  private void run(GctpServiceFacade facade, String username, String password) throws UnsupportedEncodingException {
    facade.login(username, password);

    Gctp response = facade.invoke(
      "UDLEJER",
      "KOMK", "0101",
      "VEJK", "2556",
      "HUSNR", "042B",
      "FRA", LocalDate.now().minusMonths(12).format(DateTimeFormatter.BASIC_ISO_DATE),
      "CVR", "30714024",
      "AIA", "X",
      "MAXA", "20"
    );

    processResponse(response);
  }

  private void processResponse(Gctp gctp) throws UnsupportedEncodingException {
    GctpSystem system = gctp.getSystem();
    GctpService service = system.getService().get(0);
    CprData outputData = service.getCprData().get(0);
    Rolle soegeKriterier = outputData.getRolle().get(0);
    Rolle hovedrolle = outputData.getRolle().get(1);

    printKriterier(soegeKriterier);
    printResult(hovedrolle);
  }


  private void printKriterier(Rolle soegeKriterier) {
    soegeKriterier.getFieldOrTableOrPraes()
      .stream()
      .map(obj -> (Field) obj)
      .forEach(this::printField);
  }

  private void printResult(Rolle hovedrolle) {
    System.out.println();
    for (Object obj : hovedrolle.getFieldOrTableOrPraes()) {
      if (obj instanceof Field) {
        printField((Field) obj);
      } else if (obj instanceof Table) {
        printTable((Table) obj);
      }
    }
  }

  private void printTable(Table table) {
    table.getRow().forEach(this::printRow);
  }

  private void printRow(Row row) {
    System.out.println("---------------------------------------------------------------");
    row.getField().forEach(this::printField);
  }

  private void printField(Field field) {
    System.out.printf("%-22s: %s\n", field.getName(), field.getValue());
  }

}
