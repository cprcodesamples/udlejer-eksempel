package dk.cpr.facade;

import dk.cpr.GctpServiceFacade;
import dk.cpr.gctp.*;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static javax.xml.bind.Marshaller.JAXB_ENCODING;

/**
 * @author Rune Molin, rmo@nine.dk
 **/
public class DefaultGctpServiceFacade implements GctpServiceFacade {
  private static final int SIGNON_OK = 900;
  private static final String DEFAULT_ENCODING = "ISO-8859-1";
  private static final String SCHEMA_RESOURCE_NAME = "gctp_1_0.xsd";

  private final Pattern tokenPattern = Pattern.compile("^Token=(\\w*);");
  private final String endpoint;
  private final JAXBContext jc;
  private final Schema schema;
  private String token;

  public DefaultGctpServiceFacade(String endpoint) {
    this.endpoint = endpoint;
    try {
      URL url = Thread.currentThread().getContextClassLoader().getResource(SCHEMA_RESOURCE_NAME);
      if (url == null) {
        throw new IllegalStateException("Schema resource not found");
      }
      URI schemaUri = url.toURI();

      jc = JAXBContext.newInstance(Root.class);

      SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      schema = schemaFactory.newSchema(new StreamSource(schemaUri.toString()));

    } catch (URISyntaxException | JAXBException | SAXException e) {
      throw new RuntimeException(e.getMessage());
    }
  }

  @Override
  public void login(String username, String password) {
    token = null;
    Root request = createLoginRequest(username, password);

    try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
      try (CloseableHttpResponse response = sendRequestAndReceiveResponse(httpClient, marshall(request), null)) {
        checkHttpStatusIsOk(response);

        Gctp gctp = unmarshall(EntityUtils.toString(response.getEntity(), StandardCharsets.ISO_8859_1));

        if (gctp.getSik().getKvit().getValue() == SIGNON_OK) {
          String responseToken = findTokenCookie(response);

          if (responseToken == null) {
            throw new IllegalStateException("No Token received");
          }

          token = responseToken;
        } else {
          throw new IllegalStateException("Unable to perform login: " + gctp.getSik().getKvit().getText());
        }
      }
    } catch (JAXBException | IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Gctp invoke(String serviceName, String... keyvalues) {
    assert token != null;

    if (keyvalues.length % 2 != 0) {
      throw new IllegalArgumentException("Keys must be an even number of field names and values");
    }

    Root request = createServiceRequest(serviceName, keyvalues);
    try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
      try (CloseableHttpResponse response = sendRequestAndReceiveResponse(httpClient, marshall(request), token)) {
        checkHttpStatusIsOk(response);

        return unmarshall(EntityUtils.toString(response.getEntity(), StandardCharsets.ISO_8859_1));
      }
    } catch (JAXBException | IOException e) {
      throw new RuntimeException(e);
    }
  }

  private Root createLoginRequest(String username, String password) {
    return Root.builder()
      .withGctp(
        Gctp.builder().build()
      )
      .withSik(
        Sik.builder()
          .withFunction("signon")
          .withUserid(username)
          .withPassword(password)
          .build()
      )
      .build();
  }

  private Root createServiceRequest(String serviceName, String... keys) {
    Key.Builder<Void> keyBuilder = Key.builder();

    int n = 0;
    while (n < keys.length) {
      keyBuilder.addField(
        Field.builder().withName(keys[n++]).withValue(keys[n++]).build()
      );
    }

    CprServiceHeader serviceHeader = CprServiceHeader.builder()
      .withName(serviceName)
      .withKey(keyBuilder.build())
      .build();

    GctpService service = GctpService.builder()
      .withName(serviceName)
      .withCprServiceHeader(serviceHeader)
      .build();

    GctpSystem system = GctpSystem.builder().withName("CprSoeg").withService(service).build();

    Gctp gctp = Gctp.builder().withSystem(system).build();

    return Root.builder().withGctp(gctp).build();
  }

  private String findTokenCookie(HttpResponse response) {
    for (Header header : response.getHeaders("Set-Cookie")) {
      Matcher matcher = tokenPattern.matcher(header.getValue());
      if (matcher.find()) {
        return matcher.group(1);
      }
    }

    return null;
  }

  private void checkHttpStatusIsOk(HttpResponse httpResponse) {
    if (HttpStatus.SC_OK != httpResponse.getStatusLine().getStatusCode()) {
      throw new IllegalStateException("Server returned HTTP status " + httpResponse.getStatusLine().getStatusCode());
    }
  }

  private String marshall(Root root) throws JAXBException {
    StringWriter sw = new StringWriter();
    Marshaller marshaller = jc.createMarshaller();

    marshaller.setProperty(JAXB_ENCODING, DEFAULT_ENCODING);

    marshaller.marshal(root, sw);
    marshaller.setSchema(schema);
    return sw.toString();
  }

  private Gctp unmarshall(String xml) throws JAXBException {
    InputSource inputSource = new InputSource(new StringReader(xml));
    Unmarshaller unmarshaller = jc.createUnmarshaller();

    SAXSource source = new SAXSource(inputSource);

    unmarshaller.setSchema(schema);

    JAXBElement<Root> rootElement = unmarshaller.unmarshal(source, Root.class);

    return rootElement.getValue().getGctp();
  }

  private CloseableHttpResponse sendRequestAndReceiveResponse(CloseableHttpClient httpClient, String request, String token) throws IOException {
    HttpPost postRequest = new HttpPost(endpoint);

    if (token != null) {
      postRequest.setHeader("Cookie", String.format("Token=%s", token));
    }

    postRequest.setEntity(new StringEntity(request, StandardCharsets.ISO_8859_1));

    return httpClient.execute(postRequest);
  }
}
